# Инструкция по запуску

По умолчанию запускается в Firefox. Перед запуском нужно поставить соответсвтующий драйвер:

    brew install geckodriver

Запуск тестов через gradle:

    ./gradlew test

Файл с текстом со страницы сайта будет сохранён с именем такого формата в корневой директории проекта:

    2018-04-01_11-22-44__firefox_59.0.2__duckduckgo_search_engine.txt

При запуске тестов из IntelliJ IDEA файл будет сохранён в директории `.idea/modules/`
