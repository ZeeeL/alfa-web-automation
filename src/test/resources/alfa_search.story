Feature: Search Alfa-Bank

Scenario: search Alfa-Bank site

Given I go to 'https://duckduckgo.com'
When I remember search engine
And I search 'альфа банк'
And I click on the link 'https://alfabank.ru' in the search results
And I click on 'Вакансии'
And I click on 'О работе в банке'
Then I save text for employees to file
