Feature: Search products in Yandex Market

Scenario: search products in electronics section
 
Given I go to 'https://yandex.ru'
When I click on 'Маркет'
And I choose the category '[category]' in the section '[section]'
And I filter by brand '[brand]'
And I filter by price from '[price_from]' rubles to '[price_to]' rubles
And I remember the name of the first item in the list
And I click on the first item in the list
Then the name of the product displayed in the description coincides with the one I remember
 
Examples:
|section|category|brand|price_from|price_to|
|Электроника|Мобильные телефоны|Samsung|40000||
|Электроника|Наушники|Beats|17000|25000|
