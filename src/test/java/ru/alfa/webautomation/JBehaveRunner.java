package ru.alfa.webautomation;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import ru.alfa.webautomation.steps.AlfaSearchSteps;
import ru.alfa.webautomation.steps.YandexMarketSteps;


@RunWith(JUnitReportingRunner.class)
public class JBehaveRunner extends JUnitStories {

    private Configuration configuration;

    public JBehaveRunner() {
        super();
        configuration = new MostUsefulConfiguration().useFailureStrategy(new FailingUponPendingStep());
    }

    @Override
    public Configuration configuration() {
        return configuration;
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(
                configuration(),
                new YandexMarketSteps(),
                new AlfaSearchSteps()
        );
    }

    @Override
    protected List<String> storyPaths() {
        return Arrays.asList(
                "yandex_market.story",
                "alfa_search.story"
        );
    }
}
