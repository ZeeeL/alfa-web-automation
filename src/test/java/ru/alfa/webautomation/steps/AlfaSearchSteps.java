package ru.alfa.webautomation.steps;

import com.codeborne.selenide.SelenideElement;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AlfaSearchSteps extends AbstractSteps {

    private String searchEngine;


    public String getCurrentDateTime() {
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        return formatForDateNow.format(dateNow);
    }

    public String getAlfaText () {
        String text = $(By.xpath("//div[@class='message']")).text() + "\n";
        for (SelenideElement paragraph: $$(By.xpath(("//div[@class='info']/p")))) {
            text += paragraph.text() + "\n";
        }
        return text;
    }

    public String getFileName() {
        Capabilities cap = ((RemoteWebDriver) getWebDriver()).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        String version = cap.getVersion();

        return String.format(
                "%s__%s_%s__%s.txt",
                getCurrentDateTime(),
                browserName,
                version,
                searchEngine
        );
    }

    @When("I remember search engine")
    public void whenRememberSearchEngine() {
        searchEngine = title().replaceAll(" ", "_").toLowerCase();
    }

    @When("I search '$search_query'")
    public void whenSearch(@Named("search_query") String search_query) {
        $(By.name("q")).val(search_query).pressEnter();
    }

    @When("I click on the link '$link_url' in the search results")
    public void whenClickOnLinkWithUrl(@Named("link_url") String link_url) {
        $$(By.xpath(String.format("//a[contains(@href, '%s')]", link_url))).first().click();
    }

    @Then("I save text for employees to file")
    public void thenSaveText() {
        try (PrintWriter out = new PrintWriter(getFileName())) {
            out.println(getAlfaText());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
