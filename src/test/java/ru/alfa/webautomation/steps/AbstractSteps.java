package ru.alfa.webautomation.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.BeforeStory;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class AbstractSteps {
    @BeforeStory
    public void beforeStory() {
        Configuration.browser = "firefox";
    }

    @Given("I go to '$url'")
    public void givenGoToUrl(@Named("url") String url) {
        open(url);
    }

    @When("I click on '$link'")
    public void whenClickOnLink(@Named("link") String link) {
        $(By.linkText(link)).click();
    }
}
