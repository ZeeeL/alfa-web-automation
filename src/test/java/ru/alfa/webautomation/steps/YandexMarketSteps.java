package ru.alfa.webautomation.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;

import static org.junit.Assert.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selectors.byText;

public class YandexMarketSteps extends AbstractSteps{

    private String productName;

    private void preloaderFinished() {
        $(By.xpath("//div[contains(@class, 'preloadable__preloader')]"))
                .shouldBe(Condition.visible)
                .waitWhile(Condition.visible, 4000);
    }

    private void closeRegionSuggesting() {
        SelenideElement regionSuggest = $(byText("Да, спасибо"));
        if (regionSuggest.exists()) {
            executeJavaScript("arguments[0].click();",  regionSuggest);
        }
    }

    private void moveToSection (SelenideElement sectionElem) {
        String jsMouseOver = "var evObj = document.createEvent('MouseEvents');" +
                "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
                "arguments[0].dispatchEvent(evObj);";

        executeJavaScript(jsMouseOver, sectionElem);

    }

    @When("I choose the category '[category]' in the section '[section]'")
    public void whenChooseCategory(@Named("category") String category, @Named("section") String section) {
        moveToSection($(byText(section)));
        $(byText(category)).click();
    }

    @When("I filter by brand '[brand]'")
    public void whenFilterByBrand(@Named("brand") String brand) {
        $(By.xpath(String.format("//label/div/span[text()='%s']", brand))).click();
        preloaderFinished();
    }

    @When("I filter by price from '[price_from]' rubles to '[price_to]' rubles")
    public void whenFilterByPrice(@Named("price_from") String price_from, @Named("price_to") String price_to) {
        if (price_from.trim() != "") {
            $("input#glpricefrom").sendKeys(price_from);
        }

        if (price_to.trim() != "") {
            $("input#glpriceto").sendKeys(price_to);
        }
        preloaderFinished();
    }

    @When("I remember the name of the first item in the list")
    public void whenRememberProductName() {
        productName = $$(By.xpath("//div[@class='n-snippet-cell2__title']/a")).first().text();
    }

    @When("I click on the first item in the list")
    public void whenClickFirstItem() {
        $$(By.xpath("//div[@class='n-snippet-cell2__title']/a")).first().click();
    }

    @Then("the name of the product displayed in the description coincides with the one I remember")
    public void thenValidateProductName() {
        assertEquals(
                productName,
                $(By.xpath("//h1[contains(@class, 'title')]")).text()
        );
    }
}
